jQuery.sap.require("sap.ui.core.mvc.Controller");
jQuery.sap.require("sap.ca.ui.model.format.AmountFormat");

sap.ui.define(
	[
		'sap/ui/core/mvc/Controller',
		'sap/ui/model/json/JSONModel',
		'sap/viz/ui5/controls/common/feeds/FeedItem',
		'sap/viz/ui5/data/FlattenedDataset'
	], function(Controller, JSONModel, FeedItem, FlattenedDataset) {
		"use strict";

		sap.ui.core.mvc.Controller.extend("provaGrafico.view.Master", {
			onInit: function() {
			},

			onPress: function() {
				if(this._oFragment)
				{
					this._oFragment.destroy(true);
					var idPopOver = sap.ui.getCore().getElementById("idPopOver");
					if (idPopOver)
					{
						idPopOver.destroy(true);
					}
				}
				this._oFragment = sap.ui.xmlfragment("provaGrafico.view.fragments.provaGraficoFragment", this);
				var oOverlayContainer = new sap.ui.ux3.OverlayContainer();
				oOverlayContainer.addContent(this._oFragment);
				// this._oDialog = sap.ui.xmlfragment("provaGrafico.view.fragments.selectCustomers", this);

				var dataPath = "test-resources/sap/viz/demokit/dataset/milk_production_testing_data/revenue_cost_consume";
				var oVizFrame = sap.ui.getCore().getElementById("idVizFrameLine");
				oVizFrame.setVizType('line');
				oVizFrame.setUiConfig({
					"applicationSet": "fiori"
				});
				var oPopOver = sap.ui.getCore().getElementById("idPopOver");
				oPopOver.connect(oVizFrame.getVizUid());

				var oModel = new JSONModel(dataPath + "/medium.json");
				var oDataset = new FlattenedDataset({
					dimensions: [{
						name: 'Store Name',
						value: "{Store Name}"
					}],
					measures: [{
						name: 'Revenue',
						value: '{Revenue}'
					}, {
						name: 'Cost',
						value: '{Cost}'
					}],
					data: {
						path: "/milk"
					}
				});
				oVizFrame.setDataset(oDataset);
				oVizFrame.setModel(oModel);

				oVizFrame.setVizProperties({
					general: {
						layout: {
							padding: 0.04
						}
					},
					valueAxis: {
						title: {
							visible: false
						},
						label: {
							formatString: 'axisFormat'
						}
					},
					categoryAxis: {
						title: {
							visible: false
						}
					},
					plotArea: {
						dataLabel: {
							formatString: 'datalabelFormat',
							visible: true
						}
					},
					legend: {
						title: {
							visible: false
						}
					},
					title: {
						visible: false
					}
				});

				var feedValueAxis = new FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': ["Revenue"]
					}),
					feedCategoryAxis = new FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Store Name"]
					});
				oVizFrame.addFeed(feedValueAxis);
				oVizFrame.addFeed(feedCategoryAxis);
				
				oOverlayContainer.open();
				// this._oDialog.open();

			}
		});

	});